import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: {
    taiKhoan: "",
    matKhau: "",
    matKhauXacNhan: "",
    hoTen: "",
    email: "",
  },
  errors: {},
  isFormValid: false,
};
const formRegSlice = createSlice({
  name: "formReg",
  initialState,
  reducers: {
    setFormData: (state, action) => {
      state.data = { ...state.data, ...action.payload };
    },
    setFormErrors: (state, action) => {
      state.errors = { ...action.payload };
    },
    setIsValid: (state, action) => {
      state.isFormValid = action.payload;
    },
  },
});
export const { setFormData, setFormErrors, setIsValid } = formRegSlice.actions;
export default formRegSlice.reducer;
