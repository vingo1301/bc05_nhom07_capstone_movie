import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    showModalConfirm: false,
    price: 0,
    dataMovie: {},
};

const formSlice = createSlice({
  name: "purchaseRedux",
  initialState,
  reducers: {
    setShowModalTrue: (state, action) => {
      state.showModalConfirm = action.payload ;
    },
    setShowModalFalse: (state, action) => {
      state.showModalConfirm = action.payload ;
    },
    setPriceRedux: (state, action) => {
        state.price = action.payload;
    },
    setDataMovie: (state, action) => {
        state.dataMovie =  {...action.payload };
    }
    
  },
});
export const { setShowModalTrue, setShowModalFalse, setPriceRedux, setDataMovie } = formSlice.actions;
export default formSlice.reducer;
