import React, { useState } from 'react';
import { Card } from 'antd';
import { Pagination } from 'antd';
import MovieListCss from './MovieList.css'
import { NavLink } from 'react-router-dom';
import playButton from '../../img/play-button.png'
import ModalVideo from 'react-modal-video'
import { Desktop, Mobile, Tablet } from '../HOC/Responsive';

const { Meta } = Card;

export default function MovieList({movieArr}) {
    const [current, setCurrent] = useState(1);
    const [isOpen, setOpen] = useState(false)
    const [link,setLink] = useState("");
    let handleOpenModal = (linkYoututbe) => {
      setOpen(true);
      setLink(linkYoututbe);
    }
    const onChange = (page) => {
    console.log(page);
    setCurrent(page);
  };
  const renderMovieList = () => {
    let page = (current - 1)*4;
    return movieArr?.slice(page,page + 4).map((item ) => {
        return <Card
        hoverable
        style={{
          width: 240,
        }}
        cover={<img className='h-80 object-cover' alt="example" src={item.hinhAnh} />}
      >
        <Meta title={item.tenPhim} description={item.moTa.length<60?item.moTa:item.moTa.slice(0,59)+ " ..."} />
        <div className='movieAction'>
            <div className='playButton text-center'>
              <button onClick={() => handleOpenModal(item.trailer?.slice(29))} ><img src={playButton} alt="" /></button>
            </div>
            <div className='movieButton'>
                <NavLink className='hover:text-red-500 text-lg' to={`/detail/${item.maPhim}`}><button className='bg-red-500 text-white rounded py-2 px-4 mr-2 font-bold hover:bg-white hover:text-red-500 '>Xem Chi Tiết</button></NavLink>
            </div>
      </div>
      </Card>
      
    })
    }
  return (
    <>
      <Desktop>
      <div className='movieList grid grid-cols-4 gap-4'>{renderMovieList()}</div>
      <div className='my-5 flex justify-center'><Pagination current={current}     onChange={onChange} total={40} /></div>
      <React.Fragment>
			<ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId={link} onClose={() => setOpen(false)} />
		</React.Fragment>
      </Desktop>
      <Tablet>
      <div className='movieList grid grid-cols-2 gap-4'>{renderMovieList()}</div>
      <div className='my-5 flex justify-center'><Pagination current={current}     onChange={onChange} total={40} /></div>
      <React.Fragment>
			<ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId={link} onClose={() => setOpen(false)} />
		</React.Fragment>
      </Tablet>
      <Mobile>
      <div className='movieList grid grid-cols-1 gap-4'>{renderMovieList()}</div>
      <div className='my-5 flex justify-center'><Pagination current={current}     onChange={onChange} total={40} /></div>
      <React.Fragment>
			<ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId={link} onClose={() => setOpen(false)} />
		</React.Fragment>
      </Mobile>
    </>
    
  )
}
