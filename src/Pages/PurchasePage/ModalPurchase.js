import React from "react";
import { NavLink } from "react-router-dom";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";

export default function ModalPurchase() {
  return (
    <>
      <Desktop>
        <div id="bgModal" className="">
          <div className="modalLogin w-3/5 bg-gray-700 rounded">
            <img
              className="mt-10 mx-auto"
              width="209px"
              src="./assets/img/logo.png"
              alt=""
            />
            <div className="text-center">
              <h1 className="text-white text-2xl mt-3">
                Vui Lòng Đăng Nhập Để Đặt Vé!
              </h1>
              <div className="m-5">
                <NavLink to="/login">
                  <button className="bg-red-500 text-white text-xl rounded p-2 mr-2 hover:animate-bounce">
                    Đăng Nhập
                  </button>
                </NavLink>
                <NavLink to="/">
                  <button className="text-red-500 bg-white text-xl rounded p-2">
                    Trở Về
                  </button>
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </Desktop>
      <Tablet>
        <div id="bgModal" className="">
          <div className="modalLogin w-4/5 bg-gray-700 rounded">
            <img
              className="mt-10 mx-auto"
              width="209px"
              src="./assets/img/logo.png"
              alt=""
            />
            <div className="text-center">
              <h1 className="text-white text-2xl mt-3">
                Vui Lòng Đăng Nhập Để Đặt Vé!
              </h1>
              <div className="m-5">
                <NavLink to="/login">
                  <button className="bg-red-500 text-white text-xl rounded p-2 mr-2 hover:animate-bounce">
                    Đăng Nhập
                  </button>
                </NavLink>
                <NavLink to="/">
                  <button className="text-red-500 bg-white text-xl rounded p-2">
                    Trở Về
                  </button>
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </Tablet>
      <Mobile>
        <div id="bgModal" className="">
          <div className="modalLogin w-4/5 bg-gray-700 rounded">
            <img
              className="mt-10 mx-auto"
              width="209px"
              src="./assets/img/logo.png"
              alt=""
            />
            <div className="text-center">
              <h1 className="text-white text-2xl mt-3">
                Vui Lòng Đăng Nhập Để Đặt Vé!
              </h1>
              <div className="m-5">
                <NavLink to="/login">
                  <button className="bg-red-500 text-white text-xl rounded p-2 mr-2 hover:animate-bounce">
                    Đăng Nhập
                  </button>
                </NavLink>
                <NavLink to="/">
                  <button className="text-red-500 bg-white text-xl rounded p-2">
                    Trở Về
                  </button>
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </Mobile>
    </>
  );
}
