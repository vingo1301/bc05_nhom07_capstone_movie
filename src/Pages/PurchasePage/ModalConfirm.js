import { message } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setShowModalFalse } from "../../Redux-toolkit/slice/purchaseRedux";
import { movieService } from "../../Services/MovieService";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import { NavLink } from "react-router-dom";

export default function ModalConfirm({ ticket }) {
  let dispatch = useDispatch();
  let price = useSelector((state) => state.purchaseRedux.price);
  let dataMovie = useSelector((state) => state.purchaseRedux.dataMovie);
  let user = useSelector((state) => state.userSlice.userInfor)
  console.log("dataMovie: ", dataMovie);
  let handleHideModalConfirm = () => {
    dispatch(setShowModalFalse(false));
  };
  let handleSuccess = () => {
    setTimeout(() => {
      dispatch(setShowModalFalse(false));
      window.location.reload();
    }, 2000);
    message.success("Thanh Toán Thành Công. Thank You!");
    let data = {
      maLichChieu: dataMovie.thongTinPhim.maLichChieu,
      danhSachVe: [],
    };
    ticket.map((ve) => {
      let index = dataMovie.danhSachGhe?.findIndex((ghe) => ghe.tenGhe == ve);
      let dataVe = {
        maGhe: dataMovie.danhSachGhe[index].maGhe,
        giaVe: dataMovie.danhSachGhe[index].giaVe,
      };
      data.danhSachVe.push(dataVe);
    });
    console.log("data: ", data);

    movieService.datve(data,user.accessToken).then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  let renderPrice = () => {
    if (ticket.length == 0) {
      return (
        <>
          {" "}
          <p className="mt-5 text-3xl text-green-600 font-bold">
            Bạn Chưa Chọn Vé Nào Cả!
          </p>
          <div className="my-7">
            <button
              disabled
              className="mr-5 font-bold text-white bg-gray-500 rounded p-3"
            >
              Thanh Toán
            </button>
            <button
              onClick={handleHideModalConfirm}
              className="mr-5 font-bold text-white bg-red-500 rounded p-3 hover:bg-white hover:text-red-500"
            >
              Trở Về
            </button>
          </div>
        </>
      );
    } else {
      return (
        <>
          <p className="mt-5 text-3xl text-green-600 font-bold">
            Thanh Toán {price} Cho Ghế{" "}
            {ticket?.map((item) => {
              return <span>{item}, </span>;
            })}
          </p>
          <div className="my-7">
            <button
              onClick={handleSuccess}
              className="mr-5 font-bold text-white bg-green-500 rounded p-3 hover:bg-white hover:text-green-500"
            >
              Đồng Ý
            </button>
            <button
              onClick={handleHideModalConfirm}
              className="mr-5 font-bold text-white bg-red-500 rounded p-3 hover:bg-white hover:text-red-500"
            >
              Trở Về
            </button>
          </div>
        </>
      );
    }
  };
  return (
    <>
      <Desktop>
        <div id="modalConfirm" className="absolute w-full h-full">
          <div className="text-center confirm w-3/5 absolute rounded  bg-orange-900">
            <div className="w-full mt-3">
              <img
                className="mx-auto"
                width={"300px"}
                src="./assets/img/logo.png"
                alt=""
              />
            </div>
            {renderPrice()}
          </div>
        </div>
      </Desktop>
      <Tablet>
        <div id="modalConfirm" className="absolute w-full h-full">
          <div className="text-center confirm w-5/6 absolute rounded  bg-orange-900">
            <div className="w-full mt-3">
              <img
                className="mx-auto"
                width={"300px"}
                src="./assets/img/logo.png"
                alt=""
              />
            </div>
            {renderPrice()}
          </div>
        </div>
      </Tablet>
      <Mobile>
        <div id="modalConfirm" className="absolute w-full h-full">
          <div className="text-center confirm w-5/6 absolute rounded  bg-orange-900">
            <div className="w-full mt-3">
              <img
                className="mx-auto"
                width={"300px"}
                src="./assets/img/logo.png"
                alt=""
              />
            </div>
            {renderPrice()}
          </div>
        </div>
      </Mobile>
    </>
  );
}
