import React from "react";
import { Fade } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";

export default function AppConnect() {
  const fadeImages = [
    {
      url: "./assets/img/app/slider_banner/banner-slider-1.jpg",
    },
    {
      url: "./assets/img/app/slider_banner/banner-slider-2.jpg",
    },
    {
      url: "./assets/img/app/slider_banner/banner-slider-3.jpg",
    },
    {
      url: "./assets/img/app/slider_banner/banner-slider-4.jpg",
    },
    {
      url: "./assets/img/app/slider_banner/banner-slider-5.jpg",
    },
    {
      url: "./assets/img/app/slider_banner/banner-slider-6.jpg",
    },
  ];
  return (
    <div className="app__connect text-white">
      <div className="container mx-auto flex flex-wrap w-4/5 py-24 max-[960px]:py-8">
        <div className="w-full min-[960px]:w-1/2 my-auto max-[960px]:text-center">
          <h4 className="text-3xl font-bold leading-loose">
            Ứng dụng tiện lợi dành cho người yêu điện ảnh
          </h4>
          <p className="py-10">
            Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và
            đổi quà hấp dẫn.
          </p>
          <a
            target="_blank"
            className="bg-orange-600 rounded font-bold px-8 py-4 hover:text-white"
            href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197"
          >
            APP MIỄN PHÍ - TẢI VỀ NGAY
          </a>
          <p className="pt-5">
            TIX có hai phiên bản{" "}
            <a
              target="_blank"
              className="underline underline-offset-4 hover:underline hover:underline-offset hover:text-white"
              href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197"
            >
              IOS
            </a>{" "}
            &{" "}
            <a
              target="_blank"
              className="underline underline-offset-4 hover:underline hover:underline-offset hover:text-white"
              href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123"
            >
              Android
            </a>
          </p>
        </div>
        <div className="w-full min-[960px]:w-1/2 relative max-[960px]:mt-4">
          <img
            className="w-52 m-auto"
            src="./assets/img/app/phone.png"
            alt=""
          />
          <div className="w-full absolute top-0 left-0">
            <div className="slide-container overflow-hidden m-auto">
              <Fade>
                {fadeImages.map((fadeImage, index) => (
                  <div className="each-fade" key={index}>
                    <div className="image-container">
                      <img src={fadeImage.url} />
                    </div>
                  </div>
                ))}
              </Fade>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
