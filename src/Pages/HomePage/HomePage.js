import React, { useEffect, useState } from "react";
import { movieService } from "../../Services/MovieService";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import ListCumRap from "./CumRap/ListCumRap";
import MovieList from "./MovieList/MovieList";
import AppConnect from "./AppConnect/AppConnect";
import Footer from "./Footer/Footer";
import Banner from "./Banner/Banner";
import News from "./News/News";
import FastTicket from "./FastTicket/FastTicket";
import { useDispatch } from "react-redux";
import { setMovieList } from "../../Redux-toolkit/slice/fastTicketRedux";
import styleAppConnectNewsFooter from "../../assets/css/style.css";

export default function HomePage() {
  let dispatch = useDispatch();
  const [movieArr, setMovieArr] = useState([]);
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        console.log(res);
        setMovieArr(res.data.content);
        dispatch(setMovieList(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
    movieService
      .getDanhSachPhimTheoRap()
      .then((res) => {
        console.log("danh sach rap", res.data.content);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <>
      <Desktop>
        <Banner></Banner>
        <FastTicket></FastTicket>
        <MovieList movieArr={movieArr}></MovieList>
        <ListCumRap dataMovie={dataMovie}></ListCumRap>
        <News></News>
        <AppConnect></AppConnect>
        <Footer></Footer>
      </Desktop>
      <Tablet>
        <Banner></Banner>
        <FastTicket></FastTicket>
        <MovieList movieArr={movieArr}></MovieList>
        <News></News>
        <AppConnect></AppConnect>
        <Footer></Footer>
      </Tablet>
      <Mobile>
        <Banner></Banner>
        <FastTicket></FastTicket>
        <MovieList movieArr={movieArr}></MovieList>
        <News></News>
        <AppConnect></AppConnect>
        <Footer></Footer>
      </Mobile>
    </>
  );
}
