import React, { useState } from "react";
import { useSelector } from "react-redux";
import { movieService } from "../../../Services/MovieService";
import moment from "moment/moment";
import { message } from "antd";

export default function FastTicket() {
  let movieList = useSelector((state) => state.fastTicketRedux.movieList);
  let [rapChieu, setRapChieu] = useState({});
  let [lichChieu, setLichChieu] = useState([]);
  let [maLichChieu, setMaLichChieu] = useState("");
  let renderTenPhim = () => {
    return movieList?.map((phim) => {
      return <option value={phim.maPhim}>{phim.tenPhim}</option>;
    });
  };
  let renderRapChieu = () => {
    return rapChieu.heThongRapChieu?.map((heThongRapChieu) => {
      return heThongRapChieu.cumRapChieu?.map((cumRapChieu, index) => {
        return (
          <option value={cumRapChieu.maCumRap}>{cumRapChieu.tenCumRap}</option>
        );
      });
    });
  };
  let renderLichChieu = () => {
    if (lichChieu.length == 0) {
      return <option value={0}>Không có suất chiếu nào tại rạp này!</option>;
    } else {
      return lichChieu?.map((lichChieu) => {
        return (
          <option value={lichChieu.maLichChieu}>
            {moment(lichChieu.ngayChieuGioChieu).format("DD-MM-YYYY - hh:mm")}
          </option>
        );
      });
    }
  };
  let layThongTinLichChieu = (maCumRap) => {
    rapChieu.heThongRapChieu?.map((heThongRapChieu) => {
      return heThongRapChieu.cumRapChieu?.map((cumRapChieu) => {
        if (cumRapChieu.maCumRap == maCumRap) {
          setLichChieu(cumRapChieu.lichChieuPhim);
        }
      });
    });
  };
  let layThongTinRapChieu = (maPhim) => {
    movieService
      .getDetailMovie(maPhim)
      .then((res) => {
        console.log("rap chieu: ", res.data.content);
        setRapChieu(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  let layMaLichChieu = (maLichChieu) => {
    setMaLichChieu(maLichChieu);
  };
  let handleDatVe = () => {
    if (maLichChieu == "") {
      message.error("Vui Lòng Chọn Theo Thứ Tự Phim >> Rạp Chiếu >> Giờ Chiếu");
    } else {
      {
        window.location = `/purchase/${maLichChieu}`;
      }
    }
  };
  return (
    <div id="fastTicket" className="container flex py-3 bg-white mb-3 rounded">
      <form action="" className="w-5/6">
        <select
          onChange={(e) => layThongTinRapChieu(e.target.value)}
          id="tenPhim"
          name="tenPhim"
          className="w-1/3 text-center text-lg font-semibold"
        >
          <option value={0}>Chọn Phim</option>
          {renderTenPhim()}
        </select>
        <select
          onChange={(e) => layThongTinLichChieu(e.target.value)}
          id="rapChieu"
          name="rapChieu"
          className="w-1/3 text-center text-lg font-semibold"
        >
          <option value={0}>Chọn Rạp</option>
          {renderRapChieu()}
        </select>
        <select
          onChange={(e) => layMaLichChieu(e.target.value)}
          id="kichChieu"
          name="LichChieu"
          className="w-1/3 text-center text-lg font-semibold"
        >
          <option value={0}>Chọn Lịch Chiếu</option>
          {renderLichChieu()}
        </select>
      </form>
      <button
        onClick={handleDatVe}
        className="w-1/6 rounded bg-red-500 py-2 mx-3 text-white font-semibold"
      >
        Mua Ngay
      </button>
    </div>
  );
}
