import React from "react";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";
import MovietabCss from "./MovieTab.css";

const onChange = (key) => {
  console.log(key);
};

export default function ListCumRap({ dataMovie }) {
  const renderCumRap = () => {
    return dataMovie.map((cumRap) => {
      return {
        label: (
          <img
            className=""
            style={{ width: "60px" }}
            src={cumRap.logo}
            alt=""
          />
        ),
        key: cumRap.maHeThongRap,
        children: (
          <Tabs
            defaultActiveKey="1"
            tabPosition="left"
            onChange={onChange}
            items={cumRap.lstCumRap.map((rapPhim) => {
              return {
                label: (
                  <div className="tabList">
                    <h2 className="text-lg text-red-500 font-semibold">
                      {rapPhim.tenCumRap}
                    </h2>
                    <p className="text-neutral-900">
                      {rapPhim.diaChi.length < 60
                        ? rapPhim.diaChi
                        : rapPhim.diaChi.slice(0, 60) + "..."}
                    </p>
                  </div>
                ),
                key: rapPhim.maCumRap,
                children: (
                  <MovieTabItem movieList={rapPhim.danhSachPhim}></MovieTabItem>
                ),
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div className="bg-slate-50">
      <div className="pb-10 w-4/5 container mx-auto">
        <Tabs
          className="cum__rap"
          defaultActiveKey="1"
          tabPosition="left"
          onChange={onChange}
          items={renderCumRap()}
        />
      </div>
    </div>
  );
}
