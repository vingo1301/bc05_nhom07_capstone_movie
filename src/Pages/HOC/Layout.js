import React from 'react'
import AppConnect from '../../data/Pages/HomePage/Component/AppConnect/AppConnect'
import Footer from '../../data/Pages/HomePage/Component/Footer/Footer'
import PartnerItem from '../../data/Pages/HomePage/Component/Footer/PartnerItem'
import HomePage from '../../data/Pages/HomePage/HomePage'
import Header from '../Components/Header/Header'
import HomePageHeader from '../HomePage/HomePage'
import { Desktop, Mobile, Tablet } from './Responsive'

export default function Layout({Component}) {
  return (
    <div>
      <Desktop>
        <Header></Header>
        <HomePageHeader></HomePageHeader>
        <Component></Component>
        <HomePage></HomePage>
      </Desktop>
      <Tablet>
        <Header></Header>
        <Component></Component>
        <AppConnect></AppConnect>
        <HomePage></HomePage>
      </Tablet>
      <Mobile>
      <Header></Header>
        <Component></Component>
        <HomePage></HomePage>
      </Mobile>
    </div>
  )
}
