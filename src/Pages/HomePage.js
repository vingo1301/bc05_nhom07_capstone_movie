import React, { useEffect, useState } from 'react'
import { movieService } from '../Services/MovieService';
import ListCumRap from './CumRap/ListCumRap';
import MovieList from './MovieList/MovieList';
import News from '../data/Pages/HomePage/Component/News/News'
import { Desktop, Mobile, Tablet } from './HOC/Responsive';
import FastTicket from './Components/FastTicket/FastTicket';
import { useDispatch } from 'react-redux';
import { setMovieList } from '../Redux-toolkit/slice/fastTicketRedux';

export default function HomePage() {
    let dispatch = useDispatch();
    const [movieArr,setMovieArr] = useState([]);
    const [dataMovie,setDataMovie] = useState([]);
    useEffect(() => {
        movieService.getDanhSachPhim().then((res) => {
                console.log(res);
                setMovieArr(res.data.content);
                dispatch(setMovieList(res.data.content))
              })
              .catch((err) => {
               console.log(err);
              });
        movieService.getDanhSachPhimTheoRap().then((res) => {
                console.log("danh sach rap",res.data.content);
                setDataMovie(res.data.content);
              })
              .catch((err) => {
               console.log(err);
              });
    },[])
  return (
    <>
    <Desktop>
    <div className='bg-slate-50'>
      <div className='py-10 w-4/5 container mx-auto'>
        <FastTicket></FastTicket>
        <MovieList movieArr = {movieArr}></MovieList>
        <ListCumRap dataMovie = {dataMovie}></ListCumRap>
      </div>
    </div>
    </Desktop>
    <Tablet>
    <div className='bg-slate-50'>
      <div className=' mt-5 py-10 w-4/5 container mx-auto'>
        <FastTicket></FastTicket>
        <MovieList movieArr = {movieArr}></MovieList>
      </div>
    </div>
    </Tablet>
    <Mobile>
    <div className='bg-slate-50'>
      <div className='mt-5 py-10 w-4/5 container mx-auto'>
        <FastTicket></FastTicket>
        <MovieList movieArr = {movieArr}></MovieList>
      </div>
    </div>
    </Mobile>
    </>
  )
}
