import React from "react";

export default function NotFoundPage() {
  return (
    <div className="h-screen flex items-center">
      <div className="container text-center">
        <h4 className="text-red-500 min-[1280px]:text-4xl min-[980px]:text-3xl text-base font-bold pb-3">
          404: The page you are looking for isn’t here
        </h4>
        <p className="text-green-500 min-[1280px]:text-3xl max-[980px]:hidden text-2xl font-semibold pb-3">
          You either tried some shady route or you came here by mistake.
          Whichever it is, try using the navigation
        </p>
        <img
          className="w-3/5 mx-auto"
          src="https://demo1.cybersoft.edu.vn/static/media/undraw_page_not_found_su7k.4861ae77.svg"
          alt=""
        />
      </div>
    </div>
  );
}
