import { library } from "@fortawesome/fontawesome-svg-core";
import { faMoneyBill } from "@fortawesome/free-solid-svg-icons";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { faLock } from "@fortawesome/free-solid-svg-icons";
import { faSliders } from "@fortawesome/free-solid-svg-icons";
library.add(faMoneyBill, faUser, faLock, faSliders);
