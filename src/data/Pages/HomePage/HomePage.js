import React from "react";
import AppConnect from "./Component/AppConnect/AppConnect";
import Footer from "./Component/Footer/Footer";
import News from "./Component/News/News";

export default function HomePage() {
  return (
    <div>
      <News />
      <AppConnect />
      <Footer />
    </div>
  );
}
