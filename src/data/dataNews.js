export const dataNews = {
  dienAnh: [
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/07/tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam-15959320391357.png",
      link: "https://tix.vn/goc-dien-anh/7943-tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam",
      tieuDe: "TENET công bố ngày khởi chiếu chính thức tại Việt Nam",
      chiTiet:
        "Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo chính thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường bên ngoài Bắc Mỹ, trong đó có Việt Nam.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/07/khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan-15943683481617.jpg",
      link: "https://tix.vn/goc-dien-anh/7941-khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan",
      tieuDe: "Khi phụ nữ không còn ở thế trốn chạy của nạn nhân",
      chiTiet:
        "Là bộ phim tâm lý li kỳ với chủ đề tội phạm, Bằng Chứng Vô Hình mang đến một góc nhìn mới về hình ảnh những người phụ nữ thời hiện đại. Điều đó được thể hiện qua câu chuyện về hai người phụ nữ cùng hợp sức để vạch mặt tên tội phạm có sở thích bệnh hoạn với phụ nữ.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/07/gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland-15937528932506.png",
      link: "https://tix.vn/goc-dien-anh/7940-gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland",
      tieuDe: "Gerard Butler cùng bồ cũ Deadpool tham gia Greenland",
      chiTiet:
        "Bộ phim hành động mang đề tài tận thế Greenland: Thảm Họa Thiên Thạch đến từ nhà sản xuất của loạt phim John Wick đã tung ra trailer đầu tiên, hé lộ nội dung cốt truyện, dàn diễn viên, cùng hàng loạt đại cảnh cháy nổ hoành tráng.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/07/dien-vien-dac-biet-cua-bang-chung-vo-hinh-15937518743844.png",
      link: "https://tix.vn/goc-dien-anh/7939-dien-vien-dac-biet-cua-bang-chung-vo-hinh",
      tieuDe: "Diễn viên đặc biệt của Bằng Chứng Vô Hình",
      chiTiet:
        "Bằng Chứng Vô Hình tiết lộ thêm với khán giả một diễn viên vô cùng đặc biệt, đi diễn như đi chơi và không hề nghe theo sự chỉ đạo của đạo diễn Trịnh Đình Lê Minh. Đó chính là chú chó Ben – trợ thủ đắc lực của cô gái mù Thu (Phương Anh Đào).",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/07/pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep-15937498464029.png",
      link: "https://tix.vn/goc-dien-anh/7938-pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep",
      tieuDe: "Pee Nak 2 - Vạn kiếp thiên thu, đi tu không hết nghiệp!",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/07/loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7-15937470779379.png",
      link: "https://tix.vn/goc-dien-anh/7937-loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7",
      tieuDe: "Loạt phim kinh dị không thể bỏ lỡ trong tháng 7!",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/06/rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de-15929959532579.jpg",
      link: "https://tix.vn/goc-dien-anh/7936-rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de",
      tieuDe: "RÒM tung trailer hé lộ cuộc sống của dân chơi số đề",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/06/antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them-15929866818923.jpg",
      link: "https://tix.vn/goc-dien-anh/7935-antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them",
      tieuDe:
        "Antebellum - Trailer cuối cùng không hé lộ bất cứ thông tin gì thêm",
    },
  ],
  review: [
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/03/review-nang-3-loi-hua-cua-cha-cau-chuyen-tinh-than-cam-dong-cua-kha-nhu-va-kieu-minh-tuan-15834049872311.jpg",
      link: "https://tix.vn/review/7876-review-nang-3-loi-hua-cua-cha-cau-chuyen-tinh-than-cam-dong-cua-kha-nhu-va-kieu-minh-tuan",
      tieuDe:
        "[Review] Nắng 3: Lời Hứa Của Cha - Câu chuyện tình thân cảm động của Khả Như và Kiều Minh Tuấn",
      chiTiet:
        "Như hai phần phim trước, Nắng 3 tiếp tục mang đến câu chuyện tình cha, mẹ - con đầy nước mắt của bộ ba nhân vật chính.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/03/review-onward-khi-phep-thuat-manh-me-nhat-chinh-la-tinh-than-15832047938817.jpg",
      link: "https://tix.vn/review/7871-review-onward-khi-phep-thuat-manh-me-nhat-chinh-la-tinh-than",
      tieuDe:
        "[Review] Onward - Khi phép thuật mạnh mẽ nhất chính là tình thân",
      chiTiet:
        "Tác phẩm mới nhất của Pixar tiếp tục là câu chuyện hài hước và cảm xúc về tình cảm gia đình.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/02/review-ke-vo-hinh-con-gi-dang-so-hon-ke-giet-nguoi-benh-hoan-vo-hinh-15828835353362.jpg",
      link: "https://tix.vn/review/7868-review-ke-vo-hinh-con-gi-dang-so-hon-ke-giet-nguoi-benh-hoan-vo-hinhs",
      tieuDe:
        "[Review] Kẻ Vô Hình - Còn gì đáng sợ hơn kẻ giết người bệnh hoạn vô hình?",
      chiTiet:
        "Phiên bản hiện đại của The Invisible Man là một trong những phim kinh dị xuất sắc nhất năm nay.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/02/review-cau-be-ma-2-ban-trai-cua-be-beo-la-day-chu-dau-xa-15823608583110.jpg",
      link: "https://tix.vn/review/7861-review-cau-be-ma-2-ban-trai-cua-be-beo-la-day-chu-dau-xa",
      tieuDe: "[Review] Cậu Bé Ma 2 - Bạn trai của 'bé Beo' là đây chứ đâu xa",
      chiTiet:
        "Brahms: The Boy II có những màn hù dọa ấn tượng nhưng cái kết lại không tương xứng với phần mở đầu hứa hẹn.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/02/review-nhim-sonic-cuoi-tha-ga-cung-chang-nhim-sieu-thanh-lay-loi-15821907793369.jpg",
      link: "https://tix.vn/review/7859-review-nhim-sonic-cuoi-tha-ga-cung-chang-nhim-sieu-thanh-lay-loi",
      tieuDe:
        "[Review] Nhím Sonic - Cười thả ga cùng chàng nhím siêu thanh lầy lội",
      chiTiet: "",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/02/review-thang-nam-hanh-phuc-ta-tung-co-buong-bo-chua-bao-gio-la-viec-de-dang-15817967038683.jpg",
      link: "https://tix.vn/review/7858-review-thang-nam-hanh-phuc-ta-tung-co-buong-bo-chua-bao-gio-la-viec-de-dang",
      tieuDe:
        "[Review] Tháng Năm Hạnh Phúc Ta Từng Có - Buông bỏ chưa bao giờ là việc dễ dàng",
      chiTiet: "",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/02/review-sac-dep-doi-tra-huong-giang-ke-chuyen-doi-minh-qua-phim-anh-15817958389162.jpg",
      link: "https://tix.vn/review/7857-review-sac-dep-doi-tra-huong-giang-ke-chuyen-doi-minh-qua-phim-anh",
      tieuDe:
        "[Review] Sắc Đẹp Dối Trá - Hương Giang kể chuyện đời mình qua phim ảnh",
      chiTiet: "",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2020/02/review-birds-of-prey-15809871977193.jpg",
      link: "https://tix.vn/review/7852-review-birds-of-prey-man-lot-xac-hoanh-trang-cua-harley-quinn-va-dc",
      tieuDe:
        "[Review] Birds of Prey - Màn lột xác hoành tráng của Harley Quinn và DC",
      chiTiet: "",
    },
  ],
  khuyenMai: [
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2019/10/123phim-nhap-ma-bkt-giam-ngay-20k-khi-dat-ve-bac-kim-thang-15712976725554.jpg",
      link: "https://tix.vn/khuyen-mai/7790-123phim-nhap-ma-bkt-giam-ngay-20k-khi-dat-ve-bac-kim-thang",
      tieuDe:
        "[123Phim] NHẬP MÃ 'BKT' - Giảm ngay 20k khi đặt vé Bắc Kim Thang",
      chiTiet:
        "123Phim đồng hành cùng phim Việt - Giảm ngay 20k mỗi giao dịch khi đặt vé Bắc Kim Thang trên ứng dụng 123Phim.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2019/08/sinh-nhat-mega-gs-15663933683466.jpg",
      link: "https://tix.vn/khuyen-mai/7774-sinh-nhat-mega-gs",
      tieuDe: "Sinh Nhật Mega GS",
      chiTiet:
        "Đến hẹn lại lên, vậy là một năm nữa đã trôi qua và chúng ta lại đến tháng 8, tháng sinh nhật của Mega GS Cinemas.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2019/05/123phim-tixshop-tro-lai-qua-xin-hon-xua-15583511037699.jpg",
      link: "https://tix.vn/khuyen-mai/7741-123phim-tixshop-tro-lai-qua-xin-hon-xua",
      tieuDe: "[123Phim] TixShop trở lại, quà ‘xịn’ hơn xưa",
      chiTiet: "Nhiều Tix làm gì, để tiêu vào TixShop chứ còn chờ chi.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2019/05/galaxy-trang-thi-xem-phim-hay-say-qua-tang-15572160162243.jpg",
      link: "https://tix.vn/khuyen-mai/7732-galaxy-trang-thi-xem-phim-hay-say-qua-tang",
      tieuDe: "[Galaxy Tràng Thi] Xem Phim Hay, Say Quà Tặng",
      chiTiet:
        "Nhân dịp khai trương Galaxy Tràng Thi, Galaxy Cinema dành tặng các Stars Hà Nội loạt phần quà siêu hấp dẫn.",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2019/04/mua-2-ve-cinestar-qua-zalopay-chi-1-000d-ve-15563607309238.jpg",
      link: "https://tix.vn/khuyen-mai/7727-mua-2-ve-cinestar-qua-zalopay-chi-1-000d-ve",
      tieuDe: "Mua 2 Vé Cinestar Qua ZaloPay Chỉ 1.000đ/vé",
      chiTiet: "",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2019/04/123phim-ban-la-fan-cung-marvel-15562538560772.jpg",
      link: "https://tix.vn/khuyen-mai/7723-123phim-ban-la-fan-cung-marvel",
      tieuDe: "[123Phim] Bạn Là Fan Cứng Marvel?",
      chiTiet: "",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2019/04/galaxy-trang-thi-trai-nghiem-bom-tan-cang-dong-cang-vui-15561704693167.jpg",
      link: "https://tix.vn/khuyen-mai/7722-galaxy-trang-thi-trai-nghiem-bom-tan-cang-dong-cang-vui",
      tieuDe: "[Galaxy Tràng Thi] Trải Nghiệm Bom Tấn Càng Đông Càng Vui",
      chiTiet: "",
    },
    {
      hinhAnh:
        "https://s3img.vcdn.vn/123phim/2019/04/mua-ve-bhd-star-tren-123phim-bang-zalopay-1-000d-ve-15547979641987.jpg",
      link: "https://tix.vn/khuyen-mai/7716-mua-ve-bhd-star-tren-123phim-bang-zalopay-1-000d-ve",
      tieuDe: "Mua Vé BHD Star Trên 123Phim Bằng ZaloPay: 1.000đ/vé",
      chiTiet: "",
    },
  ],
};
