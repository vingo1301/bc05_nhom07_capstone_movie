import axios from "axios";
import { createConfig } from "./configURL";
import {
  BASE_URL,
  config,
  URL_DETAIL_MOVIE,
  URL_LICH_CHIEU,
  URL_PHONG_VE,
} from "./configURL";

export const movieService = {
  getDanhSachPhim: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
      headers: config,
    });
  },
  getDanhSachPhimTheoRap: () => {
    return axios({
      url: URL_LICH_CHIEU,
      method: "GET",
      headers: config,
    });
  },
  getDetailMovie: (maPhim) => {
    return axios({
      url: URL_DETAIL_MOVIE + maPhim,
      method: "GET",
      headers: config,
    });
  },
  getLichChieu: (maLichChieu) => {
    return axios({
      url: URL_PHONG_VE + maLichChieu,
      method: "GET",
      headers: config,
    });
  },
  datve: (data,access) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyDatVe/DatVe",
      method: "POST",
      data: data,
      headers: { TokenCybersoft: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNSIsIkhldEhhblN0cmluZyI6IjI4LzA1LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTIzMjAwMDAwMCIsIm5iZiI6MTY2MjMxMDgwMCwiZXhwIjoxNjg1Mzc5NjAwfQ.FtGbsXl4qyqTRfJrunro0mQ7b-tNs8EWbhb7JDTzloE",
            Authorization: "Bearer " + access
          }
    });
  },
  getBanner: () => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachBanner",
      method: "GET",
      headers: createConfig(),
    });
  },
};
