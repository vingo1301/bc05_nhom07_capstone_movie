import axios from "axios";
import { BASE_URL2, createConfig } from "./configURL";

export const userService = {
  postDangNhap: (dataUser) => {
    return axios({
      url: `${BASE_URL2}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  postDangKy: (dataUser) => {
    return axios({
      url: `${BASE_URL2}/api/QuanLyNguoiDung/DangKy`,
      method: "POST",
      data: dataUser,
      headers: createConfig(),
    });
  },
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL2}/api/QuanLyPhim/LayDanhSachPhim`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
