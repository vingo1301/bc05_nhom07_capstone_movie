import { userLocalService } from "../../Services/localStorageService";
import { SET_USER_INFOR } from "../constant/constant";

const initialState = {
    showModalConfirm: false,
    price: 0,
    dataMovie: {},
    userInfor: userLocalService.get,
}

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'SET_SHOW_MODAL_TRUE': {
      return {...state,showModalConfirm: true}
    }
    case 'SET_SHOW_MODAL_FALSE': {
      return {...state,showModalConfirm: false}
    }
    case 'SET_PRICE': {
      return {...state,price: payload}
    }
    case 'SET_DATA_MOVIE': {
      return {...state,dataMovie: payload}
    }
    case SET_USER_INFOR:
      return { ...state, userInfor: payload };
  default:
    return state
  }
}
