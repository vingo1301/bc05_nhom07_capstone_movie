import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { Desktop, NotDestop } from "../../HOC/Responsive";
import UserNav from "./UserNav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import logo from "../../assets/img/logo.png";
export default function Header() {
  const [showDiv, setShowDiv] = useState(false);
  const [closeDiv, setCloseDiv] = useState(false);
  const handleCloseDiv = () => {
    setCloseDiv(!closeDiv);
  };
  const renderSetting = () => {
    return (
      <>
        <div onClick={handleCloseDiv} className={closeDiv && "closeDiv1"}>
          <div className="w-[250px] h-screen top-0 left-0 z-50 fixed bg-white">
            <div className="py-5 border-b-2">
              <UserNav />
            </div>
            {/* items in header */}
            <div>
              <NavLink>
                <p className="px-3 py-3  hover:text-red-500 duration-500 text-sm font-medium ">
                  Lịch Chiếu
                </p>
              </NavLink>
              <NavLink>
                <p className="px-3 py-3  hover:text-red-500 duration-500 text-sm font-medium ">
                  Cụm Rạp
                </p>
              </NavLink>
              <NavLink>
                <p className="px-3 py-3  hover:text-red-500 duration-500 text-sm font-medium ">
                  Tin Tức
                </p>
              </NavLink>
              <NavLink>
                <p className="px-3 py-3  hover:text-red-500  duration-500 text-sm font-medium ">
                  Ứng Dụng
                </p>
              </NavLink>
            </div>
          </div>
        </div>
      </>
    );
  };
  return (
    <>
      <div className="flex justify-between items-center px-10 fixed w-full h-18 top-0 left-0 z-50 bg-white opacity-90">
        <NavLink to="/">
          <img src={logo} className="h-16 w-52" />
        </NavLink>
        <Desktop>
          <div className="flex justify-between items-center px-10 ">
            <NavLink>
              <span className="px-3 hover:text-red-500 duration-500 text-sm font-medium ">
                Lịch Chiếu
              </span>
            </NavLink>
            <NavLink>
              <span className="px-3 hover:text-red-500 duration-500 text-sm font-medium ">
                Cụm Rạp
              </span>
            </NavLink>
            <NavLink>
              <span className="px-3 hover:text-red-500 duration-500 text-sm font-medium ">
                Tin Tức
              </span>
            </NavLink>
            <NavLink>
              <span className="px-3 hover:text-red-500  duration-500 text-sm font-medium ">
                Ứng Dụng
              </span>
            </NavLink>
          </div>
          <UserNav />
        </Desktop>
        <NotDestop>
          <button onClick={() => setShowDiv(!showDiv)}>
            <FontAwesomeIcon
              className="text-xl pt-2 text-red-500 fixed top-[10px] right-[10px]"
              icon="fa-solid fa-sliders"
            />
          </button>
          {showDiv && <div>{renderSetting()}</div>}
        </NotDestop>
      </div>
    </>
  );
}
