import React from 'react'

export default function ModalTrailer({trailer}) {
  return (
    
    <div className='h-full'>
        <iframe width="100%" height="100%" src={trailer} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
  )
}
